#!/bin/sh

if [ -f ".env" ]; then
    echo ".env file alredy exists"
    exit 1
fi

cat > .env <<- EOF
DEBUG=1
TILDA_PUBLIC_KEY=
TILDA_SECRET_KEY=
EOF
