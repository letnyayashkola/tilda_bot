NAME=tilda_bot

init_env:
	./dev/create-dotenv.sh
	docker-compose -p ${NAME} -f dev/docker-compose.yaml up -d

start_env:
	docker-compose -p ${NAME} -f dev/docker-compose.yaml start

stop_env:
	docker-compose -p ${NAME} -f dev/docker-compose.yaml stop

rm_env:
	docker-compose -p ${NAME} -f dev/docker-compose.yaml rm

run:
	poetry run python run.py
