import os
from http import HTTPStatus

import aiofiles

import aiohttp
from aiohttp import ClientResponse

import asyncssh
from asyncssh import SSHClientConnection


async def update(context: dict):
    if context["projectid"] not in context["config"]["projects"]:
        context["logger"].debug("unknown project: " + context["projectid"])
        return

    if context["pageid"] not in context["config"]["projects"][context["projectid"]]:
        context["logger"].debug("unknown page: " + context["pageid"])
        return

    url = context["config"]["tilda_url"].format(context["projectid"], context["pageid"])

    context["logger"].debug(url)

    async with aiohttp.ClientSession(loop=context["loop"]) as session:
        async with session.get(url) as resp:  # type: ClientResponse
            if resp.status != HTTPStatus.OK or resp.content_type != "application/json":
                context["logger"].error(await resp.read())
                return

            payload = await resp.json()

            if payload["status"] != "FOUND":
                context["logger"].error(payload)
                return

            payload = payload["result"]

            name = context["config"]["projects"][context["projectid"]][
                context["pageid"]
            ]

            path = os.path.join(context["config"]["base_path"], name)

            await context["loop"].run_in_executor(None, os.makedirs, path, 0o777, True)

            path = os.path.join(path, "index.html")

            async with aiofiles.open(path, "w", loop=context["loop"]) as f_:
                await f_.write(payload["html"])

            del payload["html"]

            context["logger"].debug(payload)

            if context["config"]["ssh"]:
                await send_by_ssh(context, name, path)


async def send_by_ssh(context: dict, name: str, file_path: str):
    dist = os.path.join(context["config"]["ssh_dist"], name)

    opt = {}

    if context["config"]["ssh_user"]:
        opt["username"] = context["config"]["ssh_user"]

    if context["config"]["ssh_key"]:
        opt["client_keys"] = [context["config"]["ssh_key"]]

    host = context["config"]["ssh_host"]

    async with asyncssh.connect(host, **opt) as conn:  # type: SSHClientConnection
        await conn.run("mkdir -p " + dist, check=True)

        dist = os.path.join(dist, "index.html")

        await asyncssh.scp(file_path, (conn, dist))
