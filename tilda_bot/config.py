import os

from dotenv import load_dotenv


load_dotenv()


def load_config() -> dict:
    cfg = {
        "debug": int(os.getenv("DEBUG", "0")) != 0,
        "port": int(os.getenv("PORT", "8000")),
        "public_key": os.getenv("TILDA_PUBLIC_KEY", ""),
        "secret_key": os.getenv("TILDA_SECRET_KEY", ""),
        "projects": {"2206825": {"10195361": "workshops_schedule"}},
        "base_path": os.getenv("BASE_WWW_PATH", os.path.join(os.getcwd(), "data")),
        "ssh": int(os.getenv("SSH", "0")) != 0,
        "ssh_user": os.getenv("SSH_USER", ""),
        "ssh_host": os.getenv("SSH_HOST", ""),
        "ssh_key": os.getenv("SSH_KEY", ""),
        "ssh_dist": os.getenv("SSH_DIST_PATH", ""),
    }

    cfg["tilda_url"] = (
        "https://api.tildacdn.info/v1/getpagefull"
        + f"?publickey={cfg.get('public_key')}"
        + f"&secretkey={cfg.get('secret_key')}"
        + "&projectid={0}&pageid={1}"
    )

    return cfg


def check(config: dict):
    if config["public_key"] == "":
        raise ValueError("TILDA_PUBLIC_KEY should be set")

    if config["secret_key"] == "":
        raise ValueError("TILDA_SECRET_KEY should be set")

    if config["ssh"] and config["ssh_host"] == "":
        raise ValueError("SSH_HOST required if ssh enabled")

    if config["ssh"] and config["ssh_dist"] == "":
        raise ValueError("SSH_DIST_PATH required if ssh enabled")
