import logging

from aiohttp import web
from aiohttp.web import Request

from tilda_bot import config
from tilda_bot.update import update


async def update_method(request: Request):
    context = dict(request.query.items())

    try:
        context["pageid"]
        context["projectid"]
        context["published"]
        context["publickey"]
    except KeyError as e:
        request.app["logger"].error(e)
        return web.Response(text="ok")

    if context["publickey"] != request.app["config"]["public_key"]:
        request.app["logger"].error("wrong publickey")
        return web.Response(text="ok")

    context["loop"] = request.app.loop
    context["config"] = request.app["config"]
    context["logger"] = request.app["logger"]

    request.app.loop.create_task(update(context))

    return web.Response(text="ok")


def main():
    cfg = config.load_config()
    try:
        config.check(cfg)
    except ValueError as e:
        print(e)
        return

    app = web.Application()
    app.add_routes([web.get("/update", update_method)])

    lvl = logging.DEBUG if cfg["debug"] else logging.INFO

    logging.basicConfig(level=lvl)

    app["logger"] = logging.getLogger("main")
    app["config"] = cfg

    web.run_app(app, port=cfg["port"], reuse_address=True, reuse_port=True)
