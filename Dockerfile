FROM inn0kenty/pyinstaller-alpine:3.7 as builder

WORKDIR /build

RUN pip install poetry

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.create false \
    && poetry install --no-root --no-dev --no-interaction

COPY . .

RUN pyinstaller --noconfirm --clean --onefile --name app run.py

FROM alpine:3.11

LABEL maintainer="Innokenty Lebedev <i.lebedev@letnyayashkola.org>"

COPY --from=builder /build/dist/app .

ENTRYPOINT ["./app"]
